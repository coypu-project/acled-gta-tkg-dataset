## Generation of the GTA RDF data

### Preliminaries
* [TARQL](https://tarql.github.io/)
* [Apache Jena](https://jena.apache.org/) (or any other library the can compute SPARQL Update on a RDF file)
* the mapping file 
* the GTA CSV data

### Steps

1. use TARQL with the file [`gta_mapping.tarql`](./gta_mapping.tarql)

```bash
$TARQL_HOME/bin/tarql gta_mapping.tarql gta.csv > gta.ttl
```
2. (Optional) linking of the GTA jurisdiction strings to the corresponding Wikidata entities

```bash
$JENA_HOME/bin/tdb2.tdbloader --loc tdb2/gta gta.ttl
$JENA_HOME/bin/tdb2.tdbloader --loc tdb2/gta --graph https://data.coypu.org/gta/mappings/ gta_wikidata_country_mappings.ttl
$JENA_HOME/bin/tdb2.tdbupdate --loc tdb2/gta --update mapping/gta_jurisdiction_linking.ru
$JENA_HOME/bin/tdb2.tdbupdate --loc tdb2/gta "DROP GRAPH <https://data.coypu.org/gta/mappings/>"
$JENA_HOME/bin/tdb2.tdbdump --loc tdb2/gta --formatted Trig > gta_linked.ttl
```
