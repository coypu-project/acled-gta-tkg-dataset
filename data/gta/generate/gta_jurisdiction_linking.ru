PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX gta: <https://schema.coypu.org/gta#>
PREFIX data: <https://data.coypu.org/event/gta/>
PREFIX wd: <http://www.wikidata.org/entity/>
PREFIX owl: <http://www.w3.org/2002/07/owl#>


DELETE {
  ?i gta:hasImplementingJurisdiction ?gta_country .
}
INSERT {
  ?i gta:hasImplementingJurisdiction ?wd_country .
}
WHERE
{
?i a gta:Intervention ;
   gta:hasImplementingJurisdiction ?gta_country .
   
GRAPH <https://data.coypu.org/gta/mappings/> {
   ?wd_country owl:sameAs ?gta_country .
}

}
;

DELETE {
  ?i gta:hasAffectedJurisdiction ?gta_country .
}
INSERT {
  ?i gta:hasAffectedJurisdiction ?wd_country .
}
WHERE
{
?i a gta:Intervention ;
   gta:hasAffectedJurisdiction ?gta_country .
   
GRAPH <https://data.coypu.org/gta/mappings/> {
   ?wd_country owl:sameAs ?gta_country .
}

}
;
