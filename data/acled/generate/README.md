## Generation of the ACLED RDF data

### Preliminaries
* [TARQL](https://tarql.github.io/) - please use our [forked branch](https://github.com/AKSW/tarql/tree/feature/jena-js-support) which allows for JS functions in TARQL
* [Apache Jena](https://jena.apache.org/) (or any other library the can compute SPARQL Update on a RDF file)
* the mapping file 
* the ACLED CSV data

### Steps
1. use TARQL with the file [`acled.tarql`](./acled.tarql)

```bash
$TARQL_HOME/bin/tarql --set arq:js-library=functions.js acled.tarql acled.csv > acled_raw.ttl
```
2. run SPARQL update statement to get countries for the actors which are unfortunately only hidden in the actor labels

```bash
$JENA_HOME/bin/tdb2.tdbloader --loc tdb2/acled acled_raw.ttl
$JENA_HOME/bin/tdb2.tdbupdate --loc tdb2/acled --update acled_insert_actor_countries.ru
$JENA_HOME/bin/tdb2.tdbdump --loc tdb2/acled --formatted Trig > acled.ttl
```
