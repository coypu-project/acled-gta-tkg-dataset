PREFIX  bd:   <http://www.bigdata.com/rdf#>
PREFIX  rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX  wdt:  <http://www.wikidata.org/prop/direct/>
PREFIX  mwapi: <https://www.mediawiki.org/ontology#API/>
PREFIX  wikibase: <http://wikiba.se/ontology#>
PREFIX  rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX  wd:   <http://www.wikidata.org/entity/>
PREFIX coy: <https://schema.coypu.org/global#>

INSERT {
  GRAPH <https://data.coypu.org/acled/tmp1> {
    ?country rdfs:label ?c .
  }
}
WHERE {
SELECT  ?c ?country {
  	{ SELECT DISTINCT ?c
                WHERE
                  { { SELECT DISTINCT  ?al
                      WHERE
                        { ?s  <https://schema.coypu.org/global#hasActor>  ?a .
                          ?a  rdfs:label            ?al
                        }
                    }
                    BIND(if(contains(?al, "Government of"), replace(?al, "^(?:Former\\s)?Government of\\s(?:the\\s)?([a-zA-Z -]*)\\s\\(.*\\)$", "$1"), coalesce()) AS ?actor1_country1)
                    BIND(if(contains(?al, "Forces of"), replace(?al, "^(?:.*\\s)?Forces of\\s(?:the\\s)?([a-zA-Z -]*)\\s\\(.*\\).*$", "$1"), coalesce()) AS ?actor1_country2)
                    BIND(if(regex(?al, "^Unidentified.*\\([a-zA-Z -]*\\)"), replace(?al, "^Unidentified\\s(?:.*)\\(([a-zA-Z -]*)\\)$", "$1"), coalesce()) AS ?actor1_country3)
                    BIND(if(regex(?al, ".*\\(([a-zA-Z -]*)\\)$"), replace(?al, ".*\\(([a-zA-Z -]*)\\)$", "$1"), coalesce()) AS ?actor1_country4)
                    BIND(coalesce(?actor1_country1, ?actor1_country2, ?actor1_country3, ?actor1_country4) AS ?c)
                    FILTER bound(?c)
                  }
              }
              LATERAL {
		      SERVICE <https://query.wikidata.org/sparql>
		        { SERVICE wikibase:mwapi
		            { bd:serviceParam
		                        wikibase:api          "EntitySearch" ;
		                        wikibase:endpoint     "www.wikidata.org" ;
		                        mwapi:search          ?c ;
		                        mwapi:limit           5 ;
		                        mwapi:language        "en" .
		              ?item     wikibase:apiOutputItem  mwapi:item .
		              ?num      wikibase:apiOrdinal   true
		            }
		          ?item  wdt:P298  ?iso3
		          FILTER ( ?num = 0 )
		          BIND(URI(concat("https://data.coypu.org/country/", ?iso3)) AS ?country)
		        }
		    }
		
            }
}
;

INSERT {
#GRAPH <https://data.coypu.org/acled/tmp2> {
    ?a coy:hasCountryLocation ?country .
  #}
} 
WHERE {
SELECT ?a ?country {
 {SELECT ?al ?country {
 		{ SELECT ?c ?al 
                WHERE
                  { { SELECT DISTINCT  ?al
                      WHERE
                        { ?s  <https://schema.coypu.org/global#hasActor>  ?a .
                          ?a  rdfs:label            ?al
                        }
                    }
                    BIND(if(contains(?al, "Government of"), replace(?al, "^(?:Former\\s)?Government of\\s(?:the\\s)?([a-zA-Z -]*)\\s\\(.*\\)$", "$1"), coalesce()) AS ?actor1_country1)
                    BIND(if(contains(?al, "Forces of"), replace(?al, "^(?:.*\\s)?Forces of\\s(?:the\\s)?([a-zA-Z -]*)\\s\\(.*\\).*$", "$1"), coalesce()) AS ?actor1_country2)
                    BIND(if(regex(?al, "^Unidentified.*\\([a-zA-Z -]*\\)"), replace(?al, "^Unidentified\\s(?:.*)\\(([a-zA-Z -]*)\\)$", "$1"), coalesce()) AS ?actor1_country3)
                    BIND(if(regex(?al, ".*\\(([a-zA-Z -]*)\\)$"), replace(?al, ".*\\(([a-zA-Z -]*)\\)$", "$1"), coalesce()) AS ?actor1_country4)
                    BIND(coalesce(?actor1_country1, ?actor1_country2, ?actor1_country3, ?actor1_country4) AS ?c)
                    FILTER bound(?c)
                  }
              }
              GRAPH <https://data.coypu.org/acled/tmp1> {
		    ?country rdfs:label ?c .
		  }
		  }}
		  ?s  <https://schema.coypu.org/global#hasActor>  ?a .
                          ?a  rdfs:label            ?al
}
}
;

DROP GRAPH <https://data.coypu.org/acled/tmp1>;
