function toIsoDateString(str) {
	return new Date(str).toISOString();
}

function toTitleCase(str) {
  return str.toLowerCase().replace(/\b\w/g, s => s.toUpperCase());
}

function titleize(str) {
    let upper = true
    let newStr = ""
    for (let i = 0, l = str.length; i < l; i++) {
        // Note that you can also check for all kinds of spaces  with
        // str[i].match(/\s/)
        if (str[i] == " ") {
            upper = true
            newStr += str[i]
            continue
        }
        newStr += upper ? str[i].toUpperCase() : str[i].toLowerCase()
        upper = false
    }
    return newStr
}

// CamelCase a string
// Words to be combined are separated by a space in the string.

function toCamelCase(str) {
    return str.split(' ')
    .map(cc)
    .join('');
}

function ucFirst(word)    {
    return word.charAt(0).toUpperCase() + word.slice(1).toLowerCase();
}

function lcFirst(word)    {
    return word.toLowerCase();
}

function cc(word,index)   {
    return (index == 0) ? lcFirst(word) : ucFirst(word);
}
